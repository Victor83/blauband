var app = {

    config: {
        url: '/ajax',
        ajaxMethod: 'POST',
        ajaxData: null,
    },

    cellValue: '',

    init: function (){

        var self = this;
        self.config = self.config;
    },

    showMessage: function(output){

        if (typeof output.type == 'undefined'){
            output.type = 'error';
        }

        if (typeof output.title == 'undefined'){
            output.title = '';
        }

        if (typeof output.output == 'undefined'){
            output.output = 'console';
        }

        switch (output.output){
            case 'console': that.showConsoleLog(output.text);
                break;
            case 'sweetalert': that.showSweetAlert(output);
                break;
            default: that.showConsoleLog(output.text);
        }
    },

    showConsoleLog: function (message){
        console.log(message);
    },

    showSweetAlert: function (output){

        if (!output.text){
            return false;
        }
        if (typeof output.title == 'undefined'){
            output.title = '';
        }

        if (typeof output.type == 'undefined'){
            output.type = 'error';
        }

        sweetAlert(output);
    },

    setUrl: function(url){
        this.config.url = url;
    },

    setData: function(data){
        this.config.ajaxData = data;
    },

    setMethod: function(method){
        this.config.ajaxMethod = method;
    },

    setCellValue: function(value){
        this.cellValue = value;
    },

    ajaxCall: function (data){

        if (typeof data.url == "undefined"){
            data.url = '/ajax';
        }

        if (typeof data.method == "undefined"){
            data.method = 'GET';
        }

        if (typeof data.output == "undefined"){
            data.output = 'sweetalert';
        }

        if (typeof data.windowReload == "undefined"){
            data.windowReload = true;
        }

        this.setUrl(data.url);

        this.setData(data.data);

        this.setMethod(data.method);

        that = this;

        $.ajax({
            url: this.config.url,
            method: this.config.ajaxMethod,
            data: this.config.ajaxData,
            success: function (data){
                data = JSON.parse(data);

                if (data.success === null){
                    that.showMessage("Internal system error", "You have an error while send request try later", "error");
                } else {
                    if (typeof data.output != "undefined"){
                        that.showMessage(data.output);

                        var windowReload;
                        if (typeof data.output.windowReload == "undefined"){
                            windowReload = false;
                        } else {
                            windowReload = data.output.windowReload;
                        }

                        if (windowReload == true){
                            setTimeout(function(){
                                window.location.reload(true);
                            }, 2000);
                        }
                    } else {
                        window.location.reload(true);
                    }

                }

            },
        });
    },

    processEditAction: function(element){

        var value = $(element).val();

        if (this.cellValue != value){

            var row = $(element).prev('span').data('cell-row');
            var col = $(element).prev('span').data('cell-col');

            var data = {
                data: {
                    row: row,
                    col: col,
                    value: value,
                },
                url: window.location.href,
                method: 'POST',
                windowReload: false,
            };

            var url = window.location.href;

            this.ajaxCall(data);

            $(element).prev('span').text(value)

        }

        $(element).attr('type', 'hidden');
        $(element).prev('span').show();
    },
};

$(document).ready(function() {
    $(function () {
        app.init();

        /*var data = {
            'test': 'test',
            'method': app.config.ajaxMethod,
        };

        app.setData(data);*/

        $('a.sort-shevron').on('click', function (e) {
            e.preventDefault();

            var sort = $(this).data('sort');
            var sortType = $(this).data('sort-type');

            var data = {
                data: {
                    action: 'sort',
                    sort: sort,
                    sortType: sortType,
                },
            };

            app.ajaxCall(data);
        });

        $('a.erase').on('click', function(e){
            e.preventDefault();

            var action = $(this).attr('href');

            var data = {
                url: action,
                method: 'GET',
            };

            app.ajaxCall(data);

        });

        var cellValue = '';

        $('.edit-cell').on('dblclick', function(e){
            e.preventDefault();

            var row = $(this).children('span').data('cell-row');
            var col = $(this).children('span').data('cell-col');

            $('[data-cell-row=' + row + '][data-cell-col=' + col + ']').hide();
            $('input[name="cell[' + row + '][' + col + ']"]').attr('type', 'text');
            $('input[name="cell[' + row + '][' + col + ']"]').focus();
            cellValue = $('input[name="cell[' + row + '][' + col + ']"]').val();
            app.setCellValue(cellValue);

        });

        $(document).on('blur', '.edit-cell input[type="text"]', function(e){
            e.preventDefault();

            app.processEditAction(this);
        });

        $(document).on('keyup', '.edit-cell input[type="text"]', function(e){

            e.preventDefault();

            if (e.which == 13 || e.which == 27){
                app.processEditAction(this);
            }

        });
    });
});
