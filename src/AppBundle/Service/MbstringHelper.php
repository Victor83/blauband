<?php

namespace AppBundle\Service;

class MbstringHelper
{
    public function mb_str_replace($needle, $replace_text, $haystack) {

        return implode($replace_text, mb_split($needle, $haystack));

    }

}