<?php

namespace AppBundle\Service;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;

class FileHelper
{

	public $files = [];
	public $paginator;

	private $containter;
	private $request;
	private $templating;

    public function __construct(ContainerInterface $container)
    {
        $this->containter = $container;
        $this->request = $container->get('router.request_context');
        $this->templating = $container->get('templating');
	}

    public static function getFileExtension($fileName)
    {
    	if (empty($fileName)){
    		return null;
	    }

        $extension = $fileName->guessExtension();

        if ($extension === null) {
            $extension = $fileName->getClientOriginalExtension();
        }

        return $extension;
    }

	public function getDirectoryFiles($directory, $pager = 20, $skipFiles = ['.','..']) {

        $tool = $directory;
        $directory = $this->containter->getParameter('temp_directory') . '/' . $directory;
        $files = scandir($directory);

        if (is_array($files) && !empty($files)){
            foreach ($files as $index => $file) {
                if (in_array($file, $skipFiles)){
                    unset($files[$index]);
                } else {

                    $files[$index] = [
                        'id' => ($index-1),
                        'name' => $file,
                        'path' => '/download/' . $tool . '/' . $file,
                        'fullName' => $directory . '/' .$file,
                        'create' => date("Y-m-d H:i:s", filectime($directory . '/' .$file)),
                        'create_date_timestamp' => filectime($directory . '/' .$file),
                    ];
                }
            }
        }

        if (isset($_GET['page'])){
            $page = $_GET['page'];
        } else {
            $page = 1;
        }

        $currentPage = $this->setPaginator($files, $pager);

        if ($page != $currentPage){
            $page = $currentPage;
        }

        $files = array_slice($files, ($page-1)*$pager, $pager);

        $sortField = (isset($_SESSION['sortType'])) ? $_SESSION['sortType'] : "name";
        $sortOrder = (isset($_SESSION['sort'][$_SESSION['sortType']])) ? $_SESSION['sort'][$_SESSION['sortType']] : "down";

        $files = $this->sortFiles($files, $sortField, $sortOrder);

        return $files;
    }

    public function sortFiles($files, $sortField = null, $sortOrder = 'down')
    {
        usort($files, [$this, 'sortHandler']);

        if ($_SESSION['sort'][$_SESSION['sortType']] == 'up'){
            $files = array_reverse($files);
        }

        return $files;
    }

    public function sortHandler($a, $b)
    {
        $sortType = $_SESSION['sortType'];

        return strcmp($a[$sortType], $b[$sortType]);

    }

    public function setPaginator($files, $pager = 20)
    {
        if (!is_array($files)){
            return null;
        }

        $currentPageNumber = 0;

        $filesNumber = count($files);
        if ($filesNumber > 0 && $filesNumber > $pager){

            $currentPageNumber = (isset($_GET['page']) && intval($_GET['page'])) ? intval($_GET['page']) : 1;
            $pathInfo          = $this->request->getPathInfo();

            $pageNumber = ceil($filesNumber / $pager);

            if ($currentPageNumber > $pageNumber){
                $currentPageNumber = $pageNumber;
            }

            $firstPage = false;
            if ($currentPageNumber == 1){
                $firstPage = true;
            }

            $lastPage = false;
            if ($currentPageNumber == $pageNumber){
                $lastPage = true;
            }
            $params     = [
                'currentPage' => $currentPageNumber,
                'pageNumber' => $pageNumber,
                'pageLink' => $pathInfo,
                'prevPageLink' => (!$firstPage) ? $pathInfo . '?page=' . ($currentPageNumber - 1) : null,
                'nextPageLink' => (!$lastPage) ? $pathInfo . '?page=' . ($currentPageNumber + 1) : null,
            ];

            $this->paginator = $this->templating->render('default/paginator.html.twig', $params);
        } else {
            $this->paginator = null;
        }

        return $currentPageNumber;
    }
}