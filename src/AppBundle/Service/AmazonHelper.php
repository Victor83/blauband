<?php

namespace AppBundle\Service;

use \Symfony\Component\DependencyInjection\ContainerInterface;

class AmazonHelper
{

	// Save path
	const AMAZON_SAVE_PATH = '/amazon/';

    // First file data
    const AMAZON_SOURCE_DELIMETER = "\t";
    const WESTFJORD_SOURCE_DELIMETER = ";";
    const TROLLKIDS_SOURCE_DELIMETER = ",";
    const ADDITIONAL_FILE_DELIMETER = "";

    const DEFAULT_SOURCE_CHARSET = "UTF-8";
    const AMAZON_SOURCE_CHARSET = "Windows-1252";
    const ORDERS_SOURCE_CHARSET = "UTF-8";
    const ADDITIONAL_FILE_CHARSET = "UTF-8";

    // Versand document titles
    public $filesTitles = [
        'versand' => [
            'order-id',
            'sku',
            'product-name',
            'quantity-to-ship',
            'Name1',
            'Name2',
            'Adresse',
            'Stadt',
            'Postleitzahl',
            'Land',
        ],
        'Bestellung' => [
            'Bestellnummer',
            'Belegdatum',
            'Artikelnummer',
            'Artikelmenge',
        ],
        'versendet' => [
            'order-id',
            'order-item-id',
            'quantity',
            'ship-date',
            'carrier-code',
            'carrier-name',
            'tracking-number',
        ],
        'Etiketten' => [
            'Name',
            'Name 2',
            'Straße',
            'Stadt',
            'Postleitzahl',
            'Land',
            'Ref',
        ],
    ];

    // Filters
    private $availableFilters = [
        'amazon-source' => [
            'field' => 'is_prime',
            'column' => 'Y',
            'filter' => 'true',
            'filterType' => 'store',
            'filterCompare' => 'seq',
            'update_object' => false,
        ],
        'etiketten-source' => [
            'field' => 'Land',
            'column' => 'J',
            'filter' => 'DE',
            'filterType' => 'store',
            'filterCompare' => 'sneq',
            'update_object' => false,
        ],
    ];

    // Amazon columns
    const AMAZON_ORDER_COLUMN         = "A";
    const AMAZON_ORDER_ITEM_COLUMN    = "B";
    const AMAZON_SKU_COLUMN           = "K";
    const AMAZON_PRODUCT_NAME_COLUMN  = "L";
    const AMAZON_QUANTITY_COLUMN      = "O";
    const AMAZON_NAME1_COLUMN         = "Q";
    const AMAZON_NAME2_COLUMN_D       = "R";
    const AMAZON_NAME2_COLUMN_A       = "R";
    const AMAZON_ADDRESS_COLUMN_D     = "R";
    const AMAZON_ADDRESS_COLUMN_A     = "S";
    const AMAZON_SHIP_CITY_COLUMN     = "U";
    const AMAZON_POSTCODE_COLUMN      = "W";
    const AMAZON_SHIP_COUNTRY_COLUMN  = "X";
    const AMAZON_QUANTITY_PURCH_COLUMN  = "M";
    const AMAZON_RECIPIENT_NAME       = "Q";

    // Westfjord And Trollkids columns
    const WAT_ORDER_COLUMN            = "A";
    const WAT_SKU_COLUMN              = "U";
    const WAT_PRODUCT_NAME_COLUMN     = "R";
    const WAT_QUANTITY_COLUMN         = "Q";
    const WAT_NAME1_COLUMN            = "AI";
    const WAT_NAME2_COLUMN            = "AM";
    const WAT_ADDRESS_COLUMN          = "AJ";
    const WAT_SHIP_CITY_COLUMN        = "AN";
    const WAT_POSTCODE_COLUMN         = "AO";
    const WAT_SHIP_COUNTRY_COLUMN     = "AQ";

    // Etiketten columns
    const ETK_NAME1_COLUMN            = "E";
    const ETK_NAME2_COLUMN            = "F";
    const ETK_ADDRESS_COLUMN          = "G";
    const ETK_SHIP_CITY_COLUMN        = "H";
    const ETK_POSTCODE_COLUMN         = "I";
    const ETK_SHIP_COUNTRY_COLUMN     = "J";
    const ETK_SORT_COUNTRY_COLUMN     = "F";
    const ETK_SORT_NAME_COLUMN        = "A";

    // ELSend file columns
	const ELSEND_POSTCODE = "AF";
	const ELSEND_RECIPIENT = "R";
	const ELSEND_TRACKING = "B";

	// Bestellung options
    const BSG_SORT_COLUMN = "C";
    const BSG_QUANTITY_COLUMN = "D";

    // PHPExcel objects for handled files
    public $firstObject = null;
    public $secondObject = null;
    public $thirdObject = null;
    public $fourthObject = null;

    // Useful data
    public $temporaryFile;
    public $firstOrdersKeys;
    public $secondOrders;
    public $secondOrdersKeys;
    public $secondOrdersStatuses;
    public $intersectedOrders;

    // Temporary data
    public $temporaryData = [];
    public $cancellationReason = null;
    public $refundPrice = null;
    public $temporaryDirectory = null;

    protected $delimeters = [];
    protected $charsets = [];

    public function __construct(
        $firstObject = null,
        $secondObject = null,
        $fourthObject = null,
        $thirdObject = null,
        ContainerInterface $container,
        $params = []
    )
    {
        $this->setDelimetersCharsets($params);
	    if (!is_null($firstObject)) {
		    $this->firstObject = $this->prepareFile( $firstObject, $this->delimeters['first'], $this->charsets['first'] );
	    }
	    if (!is_null($secondObject)) {
		    $this->secondObject = $this->prepareFile( $secondObject, $this->delimeters['second'], $this->charsets['second'] );
	    }

        if (!is_null($thirdObject)){
            $this->thirdObject = $this->prepareFile($thirdObject, $this->delimeters['third'], $this->charsets['third']);
        }

	    if (!is_null($fourthObject)) {
		    $this->fourthObject = $this->prepareFile( $fourthObject, $this->delimeters['fourth'], $this->charsets['fourth'], 'xls' );
	    }

        $this->container = $container;
        $this->temporaryDirectory = $this->container->getParameter('temp_directory') . self::AMAZON_SAVE_PATH;

    }

    protected function setDelimetersCharsets($params = [])
    {
        if (empty($params['delimeters'])){
            $this->delimeters = [
                'first' => self::AMAZON_SOURCE_DELIMETER,
                'second' => self::WESTFJORD_SOURCE_DELIMETER,
                'third' => self::TROLLKIDS_SOURCE_DELIMETER,
                'fourth' => self::ADDITIONAL_FILE_DELIMETER,
            ];
        } else {
            $this->delimeters = [
                'first' => (isset($params['delimeters']['first'])) ? $params['delimeters']['first'] : self::AMAZON_SOURCE_DELIMETER,
                'second' => (isset($params['delimeters']['second'])) ? $params['delimeters']['second'] : self::WESTFJORD_SOURCE_DELIMETER,
                'third' => (isset($params['delimeters']['third'])) ? $params['delimeters']['third'] : self::TROLLKIDS_SOURCE_DELIMETER,
                'fourth' => (isset($params['delimeters']['fourth'])) ? $params['delimeters']['fourth'] : self::ADDITIONAL_FILE_DELIMETER,
            ];
        }

        if (empty($params['charsets'])){
            $this->charsets = [
                'first' => self::AMAZON_SOURCE_CHARSET,
                'second' => self::ORDERS_SOURCE_CHARSET,
                'third' => self::DEFAULT_SOURCE_CHARSET,
                'fourth' => self::ADDITIONAL_FILE_CHARSET,
            ];
        } else {
            $this->charsets = [
                'first' => (isset($params['charsets']['first'])) ? $params['charsets']['first'] : self::AMAZON_SOURCE_CHARSET,
                'second' => (isset($params['charsets']['second'])) ? $params['charsets']['second'] : self::WESTFJORD_SOURCE_DELIMETER,
                'third' => (isset($params['charsets']['third'])) ? $params['charsets']['third'] : self::DEFAULT_SOURCE_CHARSET,
                'fourth' => (isset($params['charsets']['fourth'])) ? $params['charsets']['fourth'] : self::ADDITIONAL_FILE_CHARSET,
            ];
        }
    }

    public function prepareFile($fileName, $delimiter = ";", $charset = 'UTF-8', $type = 'csv')
    {
    	if ($type === "csv") {
		    $csvReaderObject = \PHPExcel_IOFactory::createReader('CSV');

		    // Set custom delimeter
		    $csvReaderObject->setDelimiter($delimiter);

		    // Set custom charset
		    $csvReaderObject->setInputEncoding($charset);

		    $result = $csvReaderObject->load($fileName);
	    } elseif ($type === "xls" || $type === "xlsx"){
		    $xlsReaderObject = \PHPExcel_IOFactory::createReaderForFile($fileName);

		    $result = $xlsReaderObject->load($fileName);

	    }

        return $result;

    }

    public function process()
    {
        $this->temporaryData['firstObject'] = $this->firstObject;
        $this->temporaryData['secondObject'] = $this->secondObject;
        $this->temporaryData['thirdObject'] = $this->thirdObject;
        $this->temporaryData['fourthObject'] = $this->fourthObject;

        $versand = $this->createVersandFile();
        $this->createBestellungFile();
        $this->createVersendetFile();
        if (!is_null($versand)) {
	        $this->createEtikettenFile($versand);
        }
        return;
    }

    public function cleanData($object, $filter = [], $hasTitle = true)
    {
        if (empty($filter) || empty($object)){
            return;
        }

        if ($filter['update_object'] == false){
            $object = clone $object;
        }

        $activeSheet = $object->getActiveSheet();

        $objectDimensions = $this->getDocumentDimension($object);

        if ($filter['filterType'] == 'store'){
            $filterColumn = \PHPExcel_Cell::columnIndexFromString($filter['column'])-1;
            $startRow = 1;

            if ($hasTitle === true) {
                $startRow++;
            }

            for ($row = $startRow; $row <= $objectDimensions['rows']; ++$row){
                $value = (string)$activeSheet->getCellByColumnAndRow($filterColumn, $row)->getValue();

                if ($filter['filterCompare'] == 'seq'){
                    $statement = $value === $filter['filter'];
                } elseif ($filter['filterCompare'] == 'sneq'){
                    $statement = $value !== $filter['filter'];
                }

                if ($statement){
                    $line = $this->getLineData($row,$object);
                    $activeSheet->removeRow($row);

                    // There we should shift one position back and decrease rows count to one
                    // because if we delete one row next will be with the same index as current
                    $row--;
                    $objectDimensions['rows']--;
                }
            }

            //$objWriter = \PHPExcel_IOFactory::createWriter($object, 'Excel2007');
            //$objWriter->save('/home/victor/Documents/out23.xls');
            return $object;
        }
    }

    public function addTitles($fileName)
    {
        $result = new \PHPExcel();

        $activeSheet = $result->getActiveSheet();

        $titles = $this->filesTitles[$fileName];
        unset($this->filesTitles[$fileName]);

        foreach ($titles as $index => $title) {
            $column = \PHPExcel_Cell::stringFromColumnIndex($index);
            $this->filesTitles[$fileName][$column] = $title;
            $cell = $column . 1;
            $activeSheet->setCellValue($cell, $title);
        }

        return $result;
    }

    /**
     * @return \PHPExcel
     */
    public function createVersandFile()
    {
        $versandFile = $this->addTitles('versand');
        $mbsting = new MbstringHelper();
        $titles = $this->filesTitles['versand'];

        $resultRow = 2;

        // FIRST file
        if (!is_null($this->temporaryData['firstObject'])) {

            // Clean amazon data file by filter
            $firstCleanObject = $this->cleanData($this->firstObject, $this->availableFilters['amazon-source']);

            $this->temporaryData['firstCleanObject'] = $firstCleanObject;
            $input = $this->temporaryData['firstCleanObject'];

	        $inputDimensions = $this->getDocumentDimension($input);

	        $correspondingValues = [];
	        foreach ($titles as $column => $title) {
		        switch ($title){
			        case 'order-id': $correspondingValues[$column] = self::AMAZON_ORDER_COLUMN;
				        break;
			        case 'sku': $correspondingValues[$column] = self::AMAZON_SKU_COLUMN;
				        break;
			        case 'product-name': $correspondingValues[$column] = self::AMAZON_PRODUCT_NAME_COLUMN;
				        break;
			        case 'quantity-to-ship': $correspondingValues[$column] = self::AMAZON_QUANTITY_COLUMN;
				        break;
			        case 'Name1': $correspondingValues[$column] = self::AMAZON_NAME1_COLUMN;
				        break;
			        //TODO add condition for this field
			        case 'Name2': $correspondingValues[$column] = self::AMAZON_NAME2_COLUMN_A;
				        break;
			        //TODO add condition for this field
			        case 'Adresse': $correspondingValues[$column] = self::AMAZON_ADDRESS_COLUMN_A;
				        break;
			        case 'Stadt': $correspondingValues[$column] = self::AMAZON_SHIP_CITY_COLUMN;
				        break;
			        case 'Postleitzahl': $correspondingValues[$column] = self::AMAZON_POSTCODE_COLUMN;
				        break;
			        case 'Land': $correspondingValues[$column] = self::AMAZON_SHIP_COUNTRY_COLUMN;
				        break;
		        }
	        }
	        $activeSheet = $versandFile->getActiveSheet();
	        $inputActiveSheet = $input->getActiveSheet();
	        for ($row = 2; $row <= $inputDimensions['rows']; ++$row){
		        foreach ($titles as $column => $title) {
			        $cell = $column.$row;
			        $inputColumn = $correspondingValues[$column];
			        $inputColumn = \PHPExcel_Cell::columnIndexFromString($inputColumn)-1;
			        $inputValue = (string)$inputActiveSheet->getCellByColumnAndRow($inputColumn, $row)->getValue();
			        // Rule
			        if($title == 'Name2' and !empty($inputValue)){
				        $ShipAddress2Column = self::AMAZON_ADDRESS_COLUMN_A;
				        $ShipAddress2Column = \PHPExcel_Cell::columnIndexFromString($ShipAddress2Column)-1;
				        $ShipAddress2Value = (string)$inputActiveSheet->getCellByColumnAndRow($ShipAddress2Column, $row)->getValue();
				        if(empty($ShipAddress2Value)){
					        $inputValue = null;
				        }
			        }
			        if($title == 'Adresse'){
				        if(empty($inputValue)){
					        $ShipAddress1Column = self::AMAZON_ADDRESS_COLUMN_D;
					        $ShipAddress1Column = \PHPExcel_Cell::columnIndexFromString($ShipAddress1Column)-1;
					        $inputValue = (string)$inputActiveSheet->getCellByColumnAndRow($ShipAddress1Column, $row)->getValue();
				        }
			        }
			        // END Rule

                    // Result cell
                    $resultCell = $column.$resultRow;

			        $activeSheet->setCellValue($resultCell, $inputValue);
		        }

                // Increase result row
                $resultRow++;
	        }
        }

	    // SECOND file
        if (!is_null($this->temporaryData['secondObject'])){

	        $input = $this->temporaryData['secondObject'];

	        $inputDimensions = $this->getDocumentDimension($input);

	        $correspondingValues = [];
	        foreach ($titles as $column => $title) {
		        switch ($title){
			        case 'order-id': $correspondingValues[$column] = self::WAT_ORDER_COLUMN;
				        break;
			        case 'sku': $correspondingValues[$column] = self::WAT_SKU_COLUMN;
				        break;
			        case 'product-name': $correspondingValues[$column] = self::WAT_PRODUCT_NAME_COLUMN;
				        break;
			        case 'quantity-to-ship': $correspondingValues[$column] = self::WAT_QUANTITY_COLUMN;
				        break;
			        case 'Name1': $correspondingValues[$column] = self::WAT_NAME1_COLUMN;
				        break;
			        //TODO add condition for this field
			        case 'Name2': $correspondingValues[$column] = self::WAT_NAME2_COLUMN;
				        break;
			        //TODO add condition for this field
			        case 'Adresse': $correspondingValues[$column] = self::WAT_ADDRESS_COLUMN;
				        break;
			        case 'Stadt': $correspondingValues[$column] = self::WAT_SHIP_CITY_COLUMN;
				        break;
			        case 'Postleitzahl': $correspondingValues[$column] = self::WAT_POSTCODE_COLUMN;
				        break;
			        case 'Land': $correspondingValues[$column] = self::WAT_SHIP_COUNTRY_COLUMN;
				        break;
		        }
	        }
	        $activeSheet = $versandFile->getActiveSheet();
	        $inputActiveSheet = $input->getActiveSheet();
	        for ($row = 2; $row <= $inputDimensions['rows']; ++$row){
		        foreach ($titles as $column => $title) {
			        $cell = $column.$row;
			        $inputColumn = $correspondingValues[$column];
			        $inputColumn = \PHPExcel_Cell::columnIndexFromString($inputColumn)-1;
			        $inputValue = (string)$inputActiveSheet->getCellByColumnAndRow($inputColumn, $row)->getValue();
			        if($title == 'order-id'){
				        $order_id = $inputValue;
				        $order_column = self::WAT_ORDER_COLUMN;
			        }
			        if(in_array($title, array("Name1", "Adresse", "Stadt", "Postleitzahl", "Land")) and empty($inputValue)){
				        for ($rowm = 1; $rowm <= $row; ++$rowm){
					        $minus_row = $row-$rowm;
					        if($order_id == (string)$inputActiveSheet->getCellByColumnAndRow($order_column, $minus_row)->getValue()) {
						        $inputValue = (string)$inputActiveSheet->getCellByColumnAndRow($inputColumn, $minus_row)->getValue();
						        if(!empty($inputValue)){ break;}
					        }
					        else break;
				        }
			        }
			        if($title == 'Postleitzahl'){
				        $inputValue = $mbsting->mb_str_replace("'","", $inputValue);
			        }

                    // Result cell
                    $resultCell = $column.$resultRow;

                    $activeSheet->setCellValue($resultCell, $inputValue);

		        }

                // Increase result row
                $resultRow++;
	        }
        }

        // THIRD file
        if (!is_null($this->temporaryData['thirdObject'])) {

            $input = $this->temporaryData['thirdObject'];

            $inputDimensions = $this->getDocumentDimension($input);

            $correspondingValues = [];
            foreach ($titles as $column => $title) {
                switch ($title){
                    case 'order-id': $correspondingValues[$column] = self::WAT_ORDER_COLUMN;
                        break;
                    case 'sku': $correspondingValues[$column] = self::WAT_SKU_COLUMN;
                        break;
                    case 'product-name': $correspondingValues[$column] = self::WAT_PRODUCT_NAME_COLUMN;
                        break;
                    case 'quantity-to-ship': $correspondingValues[$column] = self::WAT_QUANTITY_COLUMN;
                        break;
                    case 'Name1': $correspondingValues[$column] = self::WAT_NAME1_COLUMN;
                        break;
                    //TODO add condition for this field
                    case 'Name2': $correspondingValues[$column] = self::WAT_NAME2_COLUMN;
                        break;
                    //TODO add condition for this field
                    case 'Adresse': $correspondingValues[$column] = self::WAT_ADDRESS_COLUMN;
                        break;
                    case 'Stadt': $correspondingValues[$column] = self::WAT_SHIP_CITY_COLUMN;
                        break;
                    case 'Postleitzahl': $correspondingValues[$column] = self::WAT_POSTCODE_COLUMN;
                        break;
                    case 'Land': $correspondingValues[$column] = self::WAT_SHIP_COUNTRY_COLUMN;
                        break;
                }
            }
            $activeSheet = $versandFile->getActiveSheet();
            $inputActiveSheet = $input->getActiveSheet();
            for ($row = 2; $row <= $inputDimensions['rows']; ++$row){
                foreach ($titles as $column => $title) {
                    $cell = $column.$row;
                    $inputColumn = $correspondingValues[$column];
                    $inputColumn = \PHPExcel_Cell::columnIndexFromString($inputColumn)-1;
                    $inputValue = (string)$inputActiveSheet->getCellByColumnAndRow($inputColumn, $row)->getValue();
                    if($title == 'order-id'){
                        $order_id = $inputValue;
                        $order_column = self::WAT_ORDER_COLUMN;
                    }
                    if(in_array($title, array("Name1", "Adresse", "Stadt", "Postleitzahl", "Land")) and empty($inputValue)){
                        for ($rowm = 1; $rowm <= $row; ++$rowm){
                            $minus_row = $row-$rowm;
                            if($order_id == (string)$inputActiveSheet->getCellByColumnAndRow($order_column, $minus_row)->getValue()) {
                                $inputValue = (string)$inputActiveSheet->getCellByColumnAndRow($inputColumn, $minus_row)->getValue();
                                if(!empty($inputValue)){ break;}
                            }
                            else break;
                        }
                    }
                    if($title == 'Postleitzahl'){
                        $inputValue = $mbsting->mb_str_replace("'","", $inputValue);
                    }

                    // Result cell
                    $resultCell = $column.$resultRow;

                    $activeSheet->setCellValue($resultCell, $inputValue);
                }

                // Increase result row
                $resultRow++;
            }

        }

         $resultFileDimensions = $this->getDocumentDimension($versandFile);

        if ($resultFileDimensions['rows'] > 1){
	        $objWriter = \PHPExcel_IOFactory::createWriter($versandFile, 'Excel2007');
	        $this->setFileVariables('versand');
	        $fileName = $this->temporaryData['files']['versand']['name'];
	        $this->saveResultFile($objWriter, $fileName);
        } else {
        	$versandFile = null;
        }

        return $versandFile;
    }


    public function createBestellungFile()
    {
        $BestellungFile = $this->addTitles('Bestellung');
        $activeSheet = $BestellungFile->getActiveSheet();

        // FIRST File

	    if (!is_null($this->temporaryData['firstObject'])){
		    $input = $this->temporaryData['firstObject'];

		    $inputDimensions = $this->getDocumentDimension($input);

		    $inputActiveSheet = $input->getActiveSheet();

		    $arMass = array();

		    for ($row = 2; $row <= $inputDimensions['rows']; ++$row){
			    $skuColumn = self::AMAZON_SKU_COLUMN;
			    $skuColumn = \PHPExcel_Cell::columnIndexFromString($skuColumn)-1;
			    $skuValue = (string)$inputActiveSheet->getCellByColumnAndRow($skuColumn, $row)->getValue();

			    $quantColumn = self::AMAZON_QUANTITY_PURCH_COLUMN;
			    $quantColumn = \PHPExcel_Cell::columnIndexFromString($quantColumn)-1;
			    $quantValue = (int)$inputActiveSheet->getCellByColumnAndRow($quantColumn, $row)->getValue();

			    if(!empty($arMass[$skuValue])){
				    $arMass[$skuValue] = $arMass[$skuValue] + $quantValue;
			    }
			    else {
				    $arMass[$skuValue] = $quantValue;
			    }

		    }
	    }

        // SECOND File

	    if (!is_null($this->temporaryData['secondObject'])){
		    $input = $this->temporaryData['secondObject'];

		    $inputDimensions = $this->getDocumentDimension($input);

		    $inputActiveSheet = $input->getActiveSheet();

		    for ($row = 2; $row <= $inputDimensions['rows']; ++$row){
			    $skuColumn = self::WAT_SKU_COLUMN;
			    $skuColumn = \PHPExcel_Cell::columnIndexFromString($skuColumn)-1;
			    $skuValue = (string)$inputActiveSheet->getCellByColumnAndRow($skuColumn, $row)->getValue();

			    $quantColumn = self::WAT_QUANTITY_COLUMN;
			    $quantColumn = \PHPExcel_Cell::columnIndexFromString($quantColumn)-1;
			    $quantValue = (int)$inputActiveSheet->getCellByColumnAndRow($quantColumn, $row)->getValue();

			    if(!empty($arMass[$skuValue])){
				    $arMass[$skuValue] = $arMass[$skuValue] + $quantValue;
			    }
			    else {
				    $arMass[$skuValue] = $quantValue;
			    }

		    }
	    }

        // THIRD File
        if (!is_null($this->temporaryData['thirdObject'])) {

            $input = $this->temporaryData['thirdObject'];

            $inputDimensions = $this->getDocumentDimension($input);

            $inputActiveSheet = $input->getActiveSheet();

            for ($row = 2; $row <= $inputDimensions['rows']; ++$row) {
                $skuColumn = self::WAT_SKU_COLUMN;
                $skuColumn = \PHPExcel_Cell::columnIndexFromString($skuColumn) - 1;
                $skuValue = (string)$inputActiveSheet->getCellByColumnAndRow($skuColumn, $row)->getValue();

                $quantColumn = self::WAT_QUANTITY_COLUMN;
                $quantColumn = \PHPExcel_Cell::columnIndexFromString($quantColumn) - 1;
                $quantValue = (int)$inputActiveSheet->getCellByColumnAndRow($quantColumn, $row)->getValue();

                if (!empty($arMass[$skuValue])) {
                    $arMass[$skuValue] = $arMass[$skuValue] + $quantValue;
                } else {
                    $arMass[$skuValue] = $quantValue;
                }

            }
        }
        // END THIRD File

	    if (!empty($arMass)){
		    $i = 2;
		    foreach ($arMass as $sku => $quant) {
			    $activeSheet->setCellValue("A".$i, '2000');
			    $activeSheet->setCellValue("B".$i, date('d/m/Y'));
			    $activeSheet->setCellValue("C".$i, $sku);
			    $activeSheet->setCellValue("D".$i, $quant);
			    $i++;
		    }

            $newBestellungFile = $this->clearAndSortEntityFile($activeSheet, self::BSG_SORT_COLUMN);

		    $objWriter = \PHPExcel_IOFactory::createWriter($newBestellungFile, 'Excel2007');
		    $this->setFileVariables('bestellung');
		    $fileName = $this->temporaryData['files']['bestellung']['name'];
		    $this->saveResultFile($objWriter, $fileName);
	    } else {
	    	$BestellungFile = null;
	    }

        return $BestellungFile;
    }



    public function createVersendetFile()
    {

        $versendetFile = $this->addTitles('versendet');
        $titles = $this->filesTitles['versendet'];

        $resultRow = 2;

	    if (!is_null($this->temporaryData['firstObject']) && !is_null($this->temporaryData['fourthObject'])){
		    $input = $this->temporaryData['firstObject'];

		    $inputDimensions = $this->getDocumentDimension($input);

		    $correspondingValues = [];
		    foreach ($titles as $column => $title) {
			    switch ($title){
				    case 'order-id': $correspondingValues[$column] = self::AMAZON_ORDER_COLUMN;
					    break;
				    case 'order-item-id': $correspondingValues[$column] = self::AMAZON_ORDER_ITEM_COLUMN;
					    break;
				    case 'quantity': $correspondingValues[$column] = self::AMAZON_QUANTITY_COLUMN;
					    break;
				    /*       case 'ship-date': $correspondingValues[$column] = self::AMAZON_QUANTITY_COLUMN;
							   break;
						   case 'carrier-code': $correspondingValues[$column] = self::AMAZON_NAME1_COLUMN;
							   break;
						   case 'carrier-name': $correspondingValues[$column] = self::AMAZON_NAME2_COLUMN_A;
							   break;
						   case 'tracking-number': $correspondingValues[$column] = self::AMAZON_NAME2_COLUMN_A;
							   break; */
			    }
		    }
		    $activeSheet = $versendetFile->getActiveSheet();
		    $inputActiveSheet = $input->getActiveSheet();
		    for ($row = 2; $row <= $inputDimensions['rows']; ++$row){
			    foreach ($titles as $column => $title) {
				    $cell = $column.$row;

				    if($title == "carrier-code"){
					    $inputValue = 'DHL';
				    }
				    elseif($title == 'carrier-name'){
					    $inputValue = '';
				    }
				    elseif($title == 'ship-date'){
					    $inputValue = date('Y-m-d');
				    }
				    elseif($title == 'tracking-number'){
					    $orderId = (string)$activeSheet->getCellByColumnAndRow(0, $row)->getValue();
					    $inputValue = $this->getTrackingNumber($orderId);
				    }
				    else {
					    $inputColumn = $correspondingValues[$column];
					    $inputColumn = \PHPExcel_Cell::columnIndexFromString($inputColumn) - 1;
					    $inputValue = (string)$inputActiveSheet->getCellByColumnAndRow($inputColumn, $row)->getValue();

				    }

                    // Result cell
                    $resultCell = $column.$resultRow;

                    $activeSheet->setCellValue($resultCell, $inputValue);

                    // Increase result row
			    }

                $resultRow++;
		    }
	    }

	    $resultFileDimensions = $this->getDocumentDimension($versendetFile);

	    if ($resultFileDimensions['rows'] > 1){
		    $objWriter = \PHPExcel_IOFactory::createWriter($versendetFile, 'CSV');
            $objWriter->setDelimiter("\t");
            $objWriter->setEnclosure("");

		    $this->setFileVariables('versendet');
		    $fileName = $this->temporaryData['files']['versendet']['name'];
		    $this->saveResultFile($objWriter, $fileName);
	    } else {
	    	$versendetFile = null;
	    }

        return $versendetFile;
    }

    public function createEtikettenFile($input)
    {
        //$input = $this->cleanData($input, $this->availableFilters['etiketten-source']);

        $etikettenFile = $this->addTitles('Etiketten');
        $titles = $this->filesTitles['Etiketten'];

        $inputDimensions = $this->getDocumentDimension($input);

        $correspondingValues = [];
        foreach ($titles as $column => $title) {
            switch ($title){
                case 'Name': $correspondingValues[$column] = self::ETK_NAME1_COLUMN;
                    break;
                case 'Name 2': $correspondingValues[$column] = self::ETK_NAME2_COLUMN;
                    break;
                case 'Straße': $correspondingValues[$column] = self::ETK_ADDRESS_COLUMN;
                    break;
                case 'Stadt': $correspondingValues[$column] = self::ETK_SHIP_CITY_COLUMN ;
                    break;
                case 'Postleitzahl': $correspondingValues[$column] = self::ETK_POSTCODE_COLUMN;
                    break;
                case 'Land': $correspondingValues[$column] = self::ETK_SHIP_COUNTRY_COLUMN;
                    break;
            }
        }

        $activeSheet = $etikettenFile->getActiveSheet();
        $inputActiveSheet = $input->getActiveSheet();
        for ($row = 2; $row <= $inputDimensions['rows']; ++$row){
            foreach ($titles as $column => $title) {
                $cell = $column.$row;
                if ($title == 'Ref'){
                    $inputValue = 'blauband';
                } else {
                    $inputColumn = $correspondingValues[$column];
                    $inputColumn = \PHPExcel_Cell::columnIndexFromString($inputColumn)-1;
                    $inputValue = (string)$inputActiveSheet->getCellByColumnAndRow($inputColumn, $row)->getValue();
                }
                $activeSheet->setCellValue($cell, $inputValue);
            }
        }

        $etikettenNewFile = $this->clearAndSortEntityFile($activeSheet, array(self::ETK_SORT_COUNTRY_COLUMN, self::ETK_SORT_NAME_COLUMN), array('A'));

        $objWriter = \PHPExcel_IOFactory::createWriter($etikettenNewFile, 'Excel2007');
	    $this->setFileVariables('etiketten');
	    $fileName = $this->temporaryData['files']['etiketten']['name'];
	    $this->saveResultFile($objWriter, $fileName);

        return $etikettenFile;
    }

    public function clearAndSortEntityFile($sheet, $sort = 'A', $checkFields = array())
    {
        $sheetDimension = $this->getDocumentDimension($sheet);

        $this->temporaryData['entityArray'] = [];
        $sortValues = [];

        // Clear records and fill in array of records for sort
        for ($row = 2; $row <= $sheetDimension['rows']; ++$row){

            $prevData = (isset($prevData)) ? $prevData : [];
            $lineData = $this->getLineData($row, $sheet);

            // Check part of fields to equality
            // TODO Make multifields checking
            $extraDelete = false;
            if (!empty($prevData) && !empty($checkFields) && is_array($checkFields)){
                foreach ($checkFields as $checkField) {
                    $checkColumnIndex = \PHPExcel_Cell::columnIndexFromString($checkField)-1;
                    if ($prevData[$checkColumnIndex] == $lineData[$checkColumnIndex]){
                        $extraDelete = true;
                    }
                }
            }

            if (!empty($prevData) && (empty(array_diff($prevData, $lineData)) || $extraDelete)){
                $sheet->removeRow($row);

                // There we should shift one position back and decrease rows count to one
                // because if we delete one row next will be with the same index as current
                $row--;
                $sheetDimension['rows']--;
            } else {
                $this->temporaryData['entityArray'][$row] = $lineData;
            }

            $prevData = $lineData;
        }

        // Sort data
        if (is_array($sort) && is_array($this->temporaryData['entityArray'])){

            // Get sort columns indexes
            $sortColumnIndexes = [];
            foreach ($sort as $sortColumnIndex => $sortColumn) {
                $sortColumnIndexes[$sortColumnIndex] = \PHPExcel_Cell::columnIndexFromString($sortColumn)-1;
            }

            $entitiesArray = [];
            foreach ($this->temporaryData['entityArray'] as $entityKey => $entity) {
                foreach ($sortColumnIndexes as $sortColumnIndex) {
                    $entitiesArray[$sortColumnIndex][$entityKey] = $entity[$sortColumnIndex];
                }
            }

            if (count($sort) == 2){
                array_multisort($entitiesArray[$sortColumnIndexes[0]], SORT_ASC, SORT_STRING,
                                $entitiesArray[$sortColumnIndexes[1]], SORT_ASC, SORT_NATURAL|SORT_FLAG_CASE);

                $resultEntityArray = [];
                foreach ($this->temporaryData['entityArray'] as $entityKey => $entity) {
                    $searchValue1 = $entity[$sortColumnIndexes[0]];
                    $searchValue2 = $entity[$sortColumnIndexes[1]];

                    if (($foundKey = array_search($searchValue2, $entitiesArray[$sortColumnIndexes[1]])) !== false){
                        if ($searchValue1 == $entitiesArray[$sortColumnIndexes[0]][$foundKey]){
                            unset($entitiesArray[$sortColumnIndexes[0]][$foundKey]);
                            unset($entitiesArray[$sortColumnIndexes[1]][$foundKey]);
                            $resultEntityArray[$foundKey] = $entity;
                        }
                    }
                }
            }
            ksort($resultEntityArray);
            $this->temporaryData['entityArray'] = $resultEntityArray;
        } else {
            for ($row = 2; $row <= $sheetDimension['rows']; ++$row){
                $sortFieldIndex = \PHPExcel_Cell::columnIndexFromString($sort)-1;
                $sortValues[$row] = $lineData[$sortFieldIndex];
            }

            array_multisort($sortValues, SORT_ASC, SORT_STRING, $this->temporaryData['entityArray']);
        }

        // Get title line data and unshift it to result array
        $titleLine = $this->getLineData(1, $sheet);
        array_unshift($this->temporaryData['entityArray'], $titleLine);
        $newExcel = new \PHPExcel();
        $newSheet = $newExcel->getActiveSheet();

        // Override data by sorted ones
        $newSheet->fromArray($this->temporaryData['entityArray'], null, 'A1', true);

        return $newExcel;
    }

    /**
     * @return resource Excel document ready to import
     */
    public function saveFile($fileDirectory)
    {
        $result = null;

        // Process first file
        //TODO make something with this hardcoded value bellow
        $this->firstOrders = $this->getOrders($this->firstObject, 4);
        $this->firstOrdersKeys = array_flip($this->firstOrders);

        // Process second file
        $secondDocumentDimension = $this->getDocumentDimension($this->secondObject);
        $this->secondOrders = $this->getOrders($this->secondObject, 0);
        $this->secondOrderStatuses = $this->getOrderStatuses($this->secondObject, 1);

        $this->secondOrdersKeys = array_flip($this->secondOrders);
        //$secondOrderStatusesKeys = $this->getRowStatus($secondOrderStatuses);

        $this->intersectedOrders = array_intersect($this->firstOrders, $this->secondOrders);

        $result = $this->getResultDocument();

        $objWriter = \PHPExcel_IOFactory::createWriter($result, 'Excel5');

        if (!is_dir($fileDirectory . '/result')){
            mkdir($fileDirectory . '/result');
        }

        //$objWriter->save($fileDirectory . '/result/file_' . date("Y_m_d") . '.xls');
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="file_' . date("Y_m_d") . '.xls"');
        header('Cache-Control: max-age=0');
        $objWriter->save('php://output');

        return $result;
    }

    /**
     * Get maxRows and maxColumns for document
     *
     * @param $document object Document object
     * @return array Dimension of the document
     */
    public function getDocumentDimension($document)
    {

        if (!$document instanceof \PHPExcel_Worksheet){
            $activeSheet = $document->getActiveSheet();
        } else {
            $activeSheet = $document;
        }

        $sheetRowsCount = $activeSheet->getHighestRow();
        $sheetColumnsCount = \PHPExcel_Cell::columnIndexFromString($activeSheet->getHighestColumn());

        return array(
            'rows' => (int)$sheetRowsCount,
            'cols' => (int)$sheetColumnsCount,
        );
    }

    public function getOrders($document, $orderColumn, $hasTitle = true)
    {
        $documentDimension = $this->getDocumentDimension($document);
        $activeSheet = $document->getActiveSheet();

        $ordersId = array();
        $startRow = 1;

        if ($hasTitle === true) {
            $startRow++;
        }

        for ($row = $startRow; $row <= $documentDimension['rows']; ++$row){
            $ordersId[$row] = (string)$activeSheet->getCellByColumnAndRow($orderColumn, $row)->getValue();
        }

        return $ordersId;
    }

    public function getOrderStatuses($document, $statusColumn, $hasTitle = true)
    {
        $documentDimension = $this->getDocumentDimension($document);
        $activeSheet = $document->getActiveSheet();

        $ordersId = array();
        $startRow = 1;

        if ($hasTitle === true) {
            $startRow++;
        }

        for ($row = $startRow; $row <= $documentDimension['rows']; ++$row){
            $ordersId[$row] = $activeSheet->getCellByColumnAndRow($statusColumn, $row)->getValue();
        }

        return $ordersId;
    }

    public function getRowStatus($statuses)
    {
        $result = null;

        $resultArray = array();
        if (is_array($statuses)){

        }
    }

    public function getResultDocument()
    {
        $result = new \PHPExcel();
        $titles = $this->getDocumentTitles($this->firstObject);

        $result = $this->setDocumentTitles($result, $titles);

        $intersectedData = $this->getIntersectedData($result);

        $result = $this->addIntersectedDataToDocument($result, $intersectedData);

        return $result;
    }

    public function getDocumentTitles($document)
    {
        $documentDimensions = $this->getDocumentDimension($document);
        $activeSheet = $document->getActiveSheet();

        $titles = array();
        for ($col = 0; $col < $documentDimensions['cols']; $col++){
            $titles[$col] = $activeSheet->getCellByColumnAndRow($col, 1)->getValue();
        }

        return $titles;
    }

    public function setDocumentTitles(&$document, $titles)
    {
        $activeSheet = $document->getActiveSheet();

        if (is_array($titles)){
            foreach ($titles as $index => $title) {
                $column = \PHPExcel_Cell::stringFromColumnIndex($index);
                $cell = $column . 1;
                $activeSheet->setCellValue($cell, $title);
            }
        }

        return $document;
    }

    public function getIntersectedData(&$document)
    {
        $resultArray = array();
        if (is_array($this->intersectedOrders) && !empty($this->intersectedOrders)){
            foreach ($this->intersectedOrders as $orderIndex => $intersectedOrder) {
                if (
                    array_key_exists($intersectedOrder, $this->firstOrdersKeys)
                    && array_key_exists($intersectedOrder, $this->secondOrdersKeys)
                ) {

                    $affiliLine = array_search($intersectedOrder, $this->firstOrders);
                    $pvsLine = array_search($intersectedOrder, $this->secondOrders);

                    $resultAffili = $this->getLineData($affiliLine, $this->firstObject);
                    $resultPVS = $this->getLineData($pvsLine, $this->secondObject);

                    $result = array(
                        'affili' => array(
                            $affiliLine => $resultAffili
                        ),
                        'pvs' => array(
                            $pvsLine => $resultPVS
                        ),
                    );

                    if (($lineData = $this->processLogic($result)) !== null){
                        $resultArray[] = $lineData;
                    }
                }

            }
        }

        return $resultArray;
    }

    public function getLineData($line, $document)
    {
        if (!$document instanceof \PHPExcel_Worksheet){
            $activeSheet = $document->getActiveSheet();
        } else {
            $activeSheet = $document;
        }

        $lastColumn = $activeSheet->getHighestColumn();

        $data = $activeSheet->rangeToArray('A' . $line . ':' . $lastColumn . $line);

        if (count($data) == 1 ) {
            return current($data);
        }

        return $data;
    }

    public function setResultLine($data)
    {
        reset($data);
        $data = current($data);

        if (isset($this->refundPrice)){
            $priceColumnIndex = \PHPExcel_Cell::columnIndexFromString(self::AFFILI_PRICE_COLUMN)-1;

            $data[$priceColumnIndex] = $this->refundPrice;
        }

        if (isset($this->cancellationReason)){
            $reasonColumnIndex = \PHPExcel_Cell::columnIndexFromString(self::AFFILI_REASON_COLUMN)-1;

            $data[$reasonColumnIndex] = $this->cancellationReason;
        }

        if (isset($this->temporaryStatus)){
            $statusColumnIndex = \PHPExcel_Cell::columnIndexFromString(self::AFFILI_STATUS_COLUMN)-1;

            $data[$statusColumnIndex] = $this->temporaryStatus;
        }

        return $data;
    }

    public function setTemporaryData($fields)
    {
        if (!empty($fields) && is_array($fields)){
            foreach ($fields as $fieldIndex => $field) {
                $this->$fieldIndex = $field;
            }
        }
    }

    public function clearTemporaryData()
    {
        $this->cancellationReason = null;
        $this->refundPrice = null;
    }

    public function addIntersectedDataToDocument(&$document, $data)
    {
        $activeSheet = $document->getActiveSheet();

        if (is_array($data)){
            foreach ($data as $index => $dataValue) {
                if (is_array($dataValue)){
                    $currentLine = $index + 2;
                    foreach ($dataValue as $lineIndex => $item) {
                        $column = \PHPExcel_Cell::stringFromColumnIndex($lineIndex);
                        $cell = $column . $currentLine;
                        $activeSheet->setCellValue($cell, $item);
                    }
                }
            }
        }

        return $document;
    }

	public function getTrackingNumber($orderId) {
		return $this->findValueInColumn($this->firstObject, $orderId, 1);
    }

	public function findValueInColumn($object, $value, $column) {
		$objectDimensions = $this->getDocumentDimension($object);

		$trackingValue = null;
		$result = null;

		$activeSheet = $object->getActiveSheet();
		for ($row = 2; $row < $objectDimensions['rows']; $row++){

			$orderValue = (string)$activeSheet->getCellByColumnAndRow(0, $row)->getValue();
			if ($orderValue == $value){
				$secondColumn = \PHPExcel_Cell::columnIndexFromString(self::AMAZON_POSTCODE_COLUMN)-1;
				$secondValue = (string)$activeSheet->getCellByColumnAndRow($secondColumn, $row)->getValue();

				$thirdColumn = \PHPExcel_Cell::columnIndexFromString(self::AMAZON_RECIPIENT_NAME)-1;
				$thirdValue = (string)$activeSheet->getCellByColumnAndRow($thirdColumn, $row)->getValue();

				$result = [
					'orderId' => $value,
					'postcode' => mb_strtolower($secondValue),
					'recipient' => mb_strtolower($thirdValue),
				];

				break;
			}

		}

		$ELSendDimensions = $this->getDocumentDimension($this->fourthObject);

		$activeSheet = $this->fourthObject->getActiveSheet();

		for ($row = 2; $row < $ELSendDimensions['rows']; $row++){
			$recipientColumn = \PHPExcel_Cell::columnIndexFromString(self::ELSEND_RECIPIENT)-1;
			$recipientValue = (string)$activeSheet->getCellByColumnAndRow($recipientColumn, $row)->getValue();
			$recipientValue = mb_strtolower($recipientValue);

			$postcodeColumn = \PHPExcel_Cell::columnIndexFromString(self::ELSEND_POSTCODE)-1;
			$postcodeValue = (string)$activeSheet->getCellByColumnAndRow($postcodeColumn, $row)->getValue();

			if ($recipientValue == $result['recipient'] && $postcodeValue == $result['postcode']){
				$trackingColumn = \PHPExcel_Cell::columnIndexFromString(self::ELSEND_TRACKING)-1;
				$trackingValue = (string)$activeSheet->getCellByColumnAndRow($trackingColumn, $row)->getValue();

				break;
			}
		}

		return $trackingValue;
    }

	public function setFileVariables($name) {
		$fileDate = date('d-m-y H-i-s');
		$this->temporaryData['files'][$name]['time'] = $fileDate;

        $extension = '.xlsx';
		if ($name == 'versendet'){
		    $extension = '.txt';
        }

		$fileDateSuffix = date('_d-M-y_H-i-s') . $extension;
		$this->temporaryData['files'][$name]['suffix'] = $fileDate;
		$this->temporaryData['files'][$name]['name'] = $name . $fileDateSuffix;
		$this->temporaryData['files'][$name]['path'] = '/download' . self::AMAZON_SAVE_PATH . $name . $fileDateSuffix;
    }

	public function saveResultFile($writer, $fileName) {
		$writer->save($this->temporaryDirectory . $fileName);
    }

    public function clearAmazonFiles()
    {
        if (!isset($this->firstObject) && !isset($this->secondObject)){
            return null;
        }

        $this->temporaryData['firstObject'] = $this->firstObject;
        $this->temporaryData['secondObject'] = $this->secondObject;

        $fileData = [];

        // FIRST file
        if (!is_null($this->temporaryData['firstObject'])) {
            $firstCleanObject = $this->cleanData($this->firstObject, $this->availableFilters['amazon-source']);

            $this->temporaryData['firstCleanObject'] = $firstCleanObject;
            $inputFirst = $this->temporaryData['firstCleanObject'];

            $activeSheetFirst = $inputFirst->getActiveSheet();

            $lastColumn = $activeSheetFirst->getHighestColumn();
            $lastRow = $activeSheetFirst->getHighestRow();

            $fileData['first'] = $activeSheetFirst->rangeToArray('A2:' . $lastColumn . $lastRow);
        }
        // End FIRST file

        // SECOND file
        if (!is_null($this->temporaryData['secondObject'])) {
            $secondCleanObject = $this->cleanData($this->secondObject, $this->availableFilters['amazon-source']);

            $this->temporaryData['secondCleanObject'] = $secondCleanObject;
            $inputSecond = $this->temporaryData['secondCleanObject'];

            $activeSheetSecond = $inputSecond->getActiveSheet();

            $lastColumnSecond = $activeSheetSecond->getHighestColumn();
            $lastRowSecond = $activeSheetSecond->getHighestRow();

            $fileData['second'] = $activeSheetSecond->rangeToArray('A2:' . $lastColumnSecond . $lastRowSecond);
        }
        // End SECOND file

        // CLear data
        if (!empty($fileData['first']) || !empty($fileData['second'])){
            $greaterFIle = 'first';
            $smallerFile = 'second';
            if (count($fileData['first']) < count($fileData['second'])){
                $greaterFIle = 'second';
                $smallerFile = 'first';
            }
        } else {
            return ["error" => "One of the files is empty"];
        }

        // if both files are not empty
        $resultData = $this->compareLInes($fileData, $greaterFIle, $smallerFile);

        $fileTitles = $activeSheetFirst->rangeToArray('A1:' . $lastColumnSecond . '1');

        $resultData = array_merge($fileTitles, $resultData);

        return $resultData;
    }

    protected function compareLines($fileData, $greaterFile, $smallerFile)
    {

        $result = [];
        $doubled = [];

        foreach ($fileData[$greaterFile] as $greaterIndex => $greaterSubarray) {
            foreach ($fileData[$smallerFile] as $smallerSubarray) {
                if ($greaterSubarray[0] == $smallerSubarray[0]){
                    $doubled[] = $greaterIndex;
                    break;
                }
            }
        }

        foreach ($fileData[$greaterFile] as $greaterIndex => $greaterFile) {
            if (!in_array($greaterIndex, $doubled)){
                $result[] = $greaterFile;
            }
        }
        return $result;
    }
}
