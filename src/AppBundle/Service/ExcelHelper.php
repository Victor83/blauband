<?php

namespace AppBundle\Service;

use \Symfony\Component\DependencyInjection\ContainerInterface;

class ExcelHelper
{

    public $container = null;

    public function __construct(ContainerInterface $container, $path = '')
    {
        $this->container = $container;
        $this->temporaryDirectory = $this->container->getParameter('temp_directory') . $path;
    }

    public function prepareFile($file, $type = 'CSV', $delimiter = ";", $charset = 'UTF-8')
    {
        $result = false;

        if ($type === "CSV") {
            $csvReaderObject = \PHPExcel_IOFactory::createReader('CSV');

            // Set custom delimeter
            $csvReaderObject->setDelimiter($delimiter);

            // Set custom charset
            $csvReaderObject->setInputEncoding($charset);

            $result = $csvReaderObject->load($file);
        } elseif ($type === "xls" || $type === "xlsx"){
            $xlsReaderObject = \PHPExcel_IOFactory::createReaderForFile($file);

            $result = $xlsReaderObject->load($file);

        }

        return $result;
    }

    public function createTableFromData($data)
    {
        if ($data instanceof \PHPExcel){
            $dimensions = $this->getDocumentDimension($data);

            if ($dimensions){
                $result = [];
                for ($row = 1; $row <= $dimensions['rows']; $row++){
                    $result[$row] = $this->getLineData($row, $data);
                }
            }
        } else {
            $result = null;
        }

        return $result;
    }

    public function getDocumentDimension($object)
    {

        if (!$object instanceof \PHPExcel_Worksheet){
            $activeSheet = $object->getActiveSheet();
        } else {
            $activeSheet = $object;
        }

        $sheetRowsCount = $activeSheet->getHighestRow();
        $sheetColumnsCount = \PHPExcel_Cell::columnIndexFromString($activeSheet->getHighestColumn());

        return array(
            'rows' => (int)$sheetRowsCount,
            'cols' => (int)$sheetColumnsCount,
        );
    }

    public function getLineData($line, $object)
    {
        if (! $object instanceof \PHPExcel_Worksheet){
            $activeSheet = $object->getActiveSheet();
        } else {
            $activeSheet = $object;
        }

        $lastColumn = $activeSheet->getHighestColumn();

        $data = $activeSheet->rangeToArray('A' . $line . ':' . $lastColumn . $line);

        if (count($data) == 1 ) {
            return current($data);
        }

        return $data;
    }

}
