<?php

namespace AppBundle\Service;

//use \PHPExcel;
//use \PHPExcel_Cell;

class AffiliHelper
{

    const AFFILI                        = "AFFILI";
    const PVS                           = "PVS";

    // PVS statuses
    const PVS_STATUS_PAYED              = "Bezahlt";
    const PVS_STATUS_COMPLETE_DELIVERY  = "Komplettlieferung";
    const PVS_STATUS_COMPLETE_RETREAT   = "Komplettretoure";
    const PVS_STATUS_PART_RETREAT       = "Teilretoure";
    const PVS_STATUS_OPEN               = "offen";
    const PVS_STATUS_CANCELLED          = "Storniert";

    // Affili statuses
    const AFFILI_APPROVED = 'Confirmed';
    const AFFILI_REMAIN = 'Open';
    const AFFILI_CANCEL = 'Cancelled';

    // Affili columns
    const AFFILI_STATUS_COLUMN          = "D";
    const AFFILI_PRICE_COLUMN           = "H";
    const AFFILI_REASON_COLUMN          = "I";

    // PVS columns
    const PVS_STATUS_COLUMN             = "B";
    const PVS_PRICE_COLUMN              = "C";

    // PHPExcel objects for handled files
    public $firstObject;
    public $secondObject;

    // Useful data
    public $firstOrders;
    public $firstOrdersKeys;
    public $secondOrders;
    public $secondOrdersKeys;
    public $secondOrdersStatuses;
    public $intersectedOrders;

    // Temporary data
    public $temporaryStatus = null;
    public $cancellationReason = null;
    public $refundPrice = null;

    public function __construct($firstObject, $secondObject)
    {
        $this->firstObject = \PHPExcel_IOFactory::load($firstObject);
        $this->secondObject = \PHPExcel_IOFactory::load($secondObject);
    }

    /**
     * @return resource Excel document ready to import
     */
    public function saveFile($fileDirectory)
    {
        $result = null;

        // Process first file
        //TODO make something with this hardcoded value bellow
        $this->firstOrders = $this->getOrders($this->firstObject, 4);
        $this->firstOrdersKeys = array_flip($this->firstOrders);

        // Process second file
        $secondDocumentDimension = $this->getDocumentDimension($this->secondObject);
        $this->secondOrders = $this->getOrders($this->secondObject, 0);
        $this->secondOrderStatuses = $this->getOrderStatuses($this->secondObject, 1);

        $this->secondOrdersKeys = array_flip($this->secondOrders);
        //$secondOrderStatusesKeys = $this->getRowStatus($secondOrderStatuses);

        $this->intersectedOrders = array_intersect($this->firstOrders, $this->secondOrders);

        $result = $this->getResultDocument();

        $objWriter = \PHPExcel_IOFactory::createWriter($result, 'Excel5');

        if (!is_dir($fileDirectory . '/result')){
            mkdir($fileDirectory . '/result');
        }

        //$objWriter->save($fileDirectory . '/result/file_' . date("Y_m_d") . '.xls');
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="file_' . date("Y_m_d") . '.xls"');
        header('Cache-Control: max-age=0');
        $objWriter->save('php://output');

        return $result;
    }

    /**
     * Get maxRows and maxColumns for document
     *
     * @param $document object Document object
     * @return array Dimension of the document
     */
    public function getDocumentDimension($document)
    {
        $activeSheet = $document->getActiveSheet();
        $sheetRowsCount = $activeSheet->getHighestRow();
        $sheetColumnsCount = \PHPExcel_Cell::columnIndexFromString($activeSheet->getHighestColumn());

        return array(
            'rows' => $sheetRowsCount,
            'cols' => $sheetColumnsCount,
        );
    }

    public function getOrders($document, $orderColumn, $hasTitle = true)
    {
        $documentDimension = $this->getDocumentDimension($document);
        $activeSheet = $document->getActiveSheet();

        $ordersId = array();
        $startRow = 1;

        if ($hasTitle === true) {
            $startRow++;
        }

        for ($row = $startRow; $row <= $documentDimension['rows']; ++$row){
            $ordersId[$row] = (string)$activeSheet->getCellByColumnAndRow($orderColumn, $row)->getValue();
        }

        return $ordersId;
    }

    public function getOrderStatuses($document, $statusColumn, $hasTitle = true)
    {
        $documentDimension = $this->getDocumentDimension($document);
        $activeSheet = $document->getActiveSheet();

        $ordersId = array();
        $startRow = 1;

        if ($hasTitle === true) {
            $startRow++;
        }

        for ($row = $startRow; $row <= $documentDimension['rows']; ++$row){
            $ordersId[$row] = $activeSheet->getCellByColumnAndRow($statusColumn, $row)->getValue();
        }

        return $ordersId;
    }

    public function getRowStatus($statuses)
    {
        $result = null;

        $resultArray = array();
        if (is_array($statuses)){

        }
    }

    public function getResultDocument()
    {
        $result = new \PHPExcel();
        $titles = $this->getDocumentTitles($this->firstObject);

        $result = $this->setDocumentTitles($result, $titles);

        $intersectedData = $this->getIntersectedData($result);

        $result = $this->addIntersectedDataToDocument($result, $intersectedData);

        return $result;
    }

    public function getDocumentTitles($document)
    {
        $documentDimensions = $this->getDocumentDimension($document);
        $activeSheet = $document->getActiveSheet();

        $titles = array();
        for ($col = 0; $col < $documentDimensions['cols']; $col++){
            $titles[$col] = $activeSheet->getCellByColumnAndRow($col, 1)->getValue();
        }

        return $titles;
    }

    public function setDocumentTitles(&$document, $titles)
    {
        $activeSheet = $document->getActiveSheet();

        if (is_array($titles)){
            foreach ($titles as $index => $title) {
                $column = \PHPExcel_Cell::stringFromColumnIndex($index);
                $cell = $column . 1;
                $activeSheet->setCellValue($cell, $title);
            }
        }

        return $document;
    }

    public function getIntersectedData(&$document)
    {
        $resultArray = array();
        if (is_array($this->intersectedOrders) && !empty($this->intersectedOrders)){
            foreach ($this->intersectedOrders as $orderIndex => $intersectedOrder) {
                if (
                    array_key_exists($intersectedOrder, $this->firstOrdersKeys)
                    && array_key_exists($intersectedOrder, $this->secondOrdersKeys)
                ) {

                    $affiliLine = array_search($intersectedOrder, $this->firstOrders);
                    $pvsLine = array_search($intersectedOrder, $this->secondOrders);

                    $resultAffili = $this->getLineData($affiliLine, $this->firstObject);
                    $resultPVS = $this->getLineData($pvsLine, $this->secondObject);

                    $result = array(
                        'affili' => array(
                            $affiliLine => $resultAffili
                        ),
                        'pvs' => array(
                            $pvsLine => $resultPVS
                        ),
                    );

                    if (($lineData = $this->processLogic($result)) !== null){
                        $resultArray[] = $lineData;
                    }
                }

            }
        }

        return $resultArray;
    }

    public function getLineData($line, $document)
    {
        $activeSheet = $document->getActiveSheet();

        $lastColumn = $activeSheet->getHighestColumn();

        $data = $activeSheet->rangeToArray('A' . $line . ':' . $lastColumn . $line);

        if (count($data) == 1 ) {
            return current($data);
        }

        return $data;
    }

    public function processLogic($lineData)
    {
        $affiliLine = key($lineData['affili']);
        $pvsLine = key($lineData['pvs']);

        $activeSheet = $this->secondObject->getActiveSheet();

        $pvsStatusColumnIndex = \PHPExcel_Cell::columnIndexFromString(self::PVS_STATUS_COLUMN)-1;
        $pvsPriceColumnIndex = \PHPExcel_Cell::columnIndexFromString(self::PVS_PRICE_COLUMN)-1;
        $orderStatus = $activeSheet->getCellByColumnAndRow($pvsStatusColumnIndex, $pvsLine)->getValue();

        $resultLineData = $this->processStatus($lineData, $orderStatus, $affiliLine);

        return $resultLineData;
    }

    public function processStatus($lineData, $orderStatus = null, $currentLine = null)
    {
        //TODO check null values $orderStatus & $currentLine

        switch ($orderStatus){

            case self::PVS_STATUS_PAYED: $result = $this->processStatusPayed($lineData, $orderStatus, $currentLine);
                break;
            case self::PVS_STATUS_COMPLETE_DELIVERY: $result = $this->processStatusCompleteDelivery($lineData, $orderStatus, $currentLine);
                break;
            case self::PVS_STATUS_OPEN: $result = $this->processStatusOpen($lineData, $orderStatus, $currentLine);
                break;
            case self::PVS_STATUS_COMPLETE_RETREAT: $result = $this->processStatusCompleteRetreat($lineData, $orderStatus, $currentLine);
                break;
            case self::PVS_STATUS_PART_RETREAT: $result = $this->processStatusPartRetreat($lineData, $orderStatus, $currentLine);
                break;
            case self::PVS_STATUS_CANCELLED: $result = $this->processStatusCancelled($lineData, $orderStatus, $currentLine);
                break;

            default: $result = $this->processStatusOpen($lineData, $orderStatus, $currentLine);
        }

        $this->clearTemporaryData();

        return $result;
    }

    public function processStatusPayed($lineData, $orderStatus, $currentLine)
    {
        if ($this->checkStatusAndLine($orderStatus, $currentLine)){
            return null;
        }

        $temporaryFields = array(
            'temporaryStatus' => self::AFFILI_APPROVED,
        );

        $this->setTemporaryData($temporaryFields);

        $result = $this->setResultLine($lineData['affili']);

        return $result;
    }

    public function processStatusCompleteDelivery($lineData, $orderStatus, $currentLine)
    {
        if ($this->checkStatusAndLine($orderStatus, $currentLine)){
            return null;
        }

        $temporaryFields = array(
            'temporaryStatus' => self::AFFILI_REMAIN,
        );

        $this->setTemporaryData($temporaryFields);

        $result = $this->setResultLine($lineData['affili']);

        return $result;
    }

    public function processStatusOpen($lineData, $orderStatus, $currentLine)
    {
        if ($this->checkStatusAndLine($orderStatus, $currentLine)){
            return null;
        }

        $temporaryFields = array(
            'temporaryStatus' => self::AFFILI_REMAIN,
        );

        $this->setTemporaryData($temporaryFields);

        $result = $this->setResultLine($lineData['affili']);

        return $result;
    }

    public function processStatusCompleteRetreat($lineData, $orderStatus, $currentLine)
    {
        if ($this->checkStatusAndLine($orderStatus, $currentLine)){
            return null;
        }

        $temporaryFields = array(
            'temporaryStatus' => self::AFFILI_CANCEL,
            'cancellationReason' => 'Komplettretoure',
        );

        $this->setTemporaryData($temporaryFields);

        $result = $this->setResultLine($lineData['affili']);

        return $result;
    }

    public function processStatusPartRetreat($lineData, $orderStatus, $currentLine)
    {
        if ($this->checkStatusAndLine($orderStatus, $currentLine)){
            return null;
        }

        $lineData['pvs'] = current($lineData['pvs']);
        $priceFromPVSIndes = \PHPExcel_Cell::columnIndexFromString(self::PVS_PRICE_COLUMN)-1;
        $refundPrice = $lineData['pvs'][$priceFromPVSIndes];
        $refundPrice = str_replace(',', '.', $refundPrice);

        $temporaryFields = array(
            'temporaryStatus' => self::AFFILI_CANCEL,
            'cancellationReason' => 'Teilretoure',
            'refundPrice' => $refundPrice,
        );

        $this->setTemporaryData($temporaryFields);

        $result = $this->setResultLine($lineData['affili']);

        return $result;
    }

    public function processStatusCancelled($lineData, $orderStatus, $currentLine)
    {
        if ($this->checkStatusAndLine($orderStatus, $currentLine)){
            return null;
        }

        $temporaryFields = array(
            'temporaryStatus' => self::AFFILI_CANCEL,
            'cancellationReason' => 'Storniert',
        );

        $this->setTemporaryData($temporaryFields);

        $result = $this->setResultLine($lineData['affili']);

        return $result;
    }

    public function checkStatusAndLine($orderStatus, $currentLine)
    {
        if (empty($orderStatus) || empty($currentLine)){
            return false;
        }
    }

    public function setResultLine($data)
    {
        reset($data);
        $data = current($data);

        if (isset($this->refundPrice)){
            $priceColumnIndex = \PHPExcel_Cell::columnIndexFromString(self::AFFILI_PRICE_COLUMN)-1;

            $data[$priceColumnIndex] = $this->refundPrice;
        }

        if (isset($this->cancellationReason)){
            $reasonColumnIndex = \PHPExcel_Cell::columnIndexFromString(self::AFFILI_REASON_COLUMN)-1;

            $data[$reasonColumnIndex] = $this->cancellationReason;
        }

        if (isset($this->temporaryStatus)){
            $statusColumnIndex = \PHPExcel_Cell::columnIndexFromString(self::AFFILI_STATUS_COLUMN)-1;

            $data[$statusColumnIndex] = $this->temporaryStatus;
        }

        return $data;
    }

    public function setTemporaryData($fields)
    {
        if (!empty($fields) && is_array($fields)){
            foreach ($fields as $fieldIndex => $field) {
                $this->$fieldIndex = $field;
            }
        }
    }

    public function clearTemporaryData()
    {
        $this->cancellationReason = null;
        $this->refundPrice = null;
    }

    /*public function __set($name, $data)
    {
        $this->$name = $data;
    }*/

    public function addIntersectedDataToDocument(&$document, $data)
    {
        $activeSheet = $document->getActiveSheet();

        if (is_array($data)){
            foreach ($data as $index => $dataValue) {
                if (is_array($dataValue)){
                    $currentLine = $index + 2;
                    foreach ($dataValue as $lineIndex => $item) {
                        $column = \PHPExcel_Cell::stringFromColumnIndex($lineIndex);
                        $cell = $column . $currentLine;
                        $activeSheet->setCellValue($cell, $item);
                    }
                }
            }
        }

        return $document;
    }
}
