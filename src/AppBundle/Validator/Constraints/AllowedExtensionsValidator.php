<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class AllowedExtensionsValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        if (null !== $value) {

            $extension = $value->getClientOriginalExtension();
            $allowedExtensions = $constraint->allowedExtensions;

            if (!in_array($extension, $allowedExtensions)){
                $this->context->buildViolation($constraint->message)
                    ->setParameters([
                        '{{ fileExtension }}' => $extension,
                        '{{ allowedExtensions }}' => implode(', ', $constraint->allowedExtensions),
                    ])
                    ->addViolation();
            }
        }

    }
}
