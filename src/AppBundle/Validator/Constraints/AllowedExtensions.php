<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class AllowedExtensions extends Constraint
{
    public $allowedExtensions = array();
    public $message = "Not allowed file extension {{ fileExtension }}. Allowed extensions are {{ allowedExtensions }}";
}