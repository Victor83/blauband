<?php

namespace AppBundle\Form;

use AppBundle\Entity\AffiliDocument;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class AffiliType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('first_file', FileType::class, array('label' => 'Affili file'))
            ->add('second_file', FileType::class, array('label' => 'PVS file'));

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => AffiliDocument::class
        ));
    }
}