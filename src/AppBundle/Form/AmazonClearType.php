<?php

namespace AppBundle\Form;

use AppBundle\Entity\AmazonClearDocument;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class AmazonClearType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('first_file', FileType::class, array('label' => 'First file to clear', 'required' => false))
            ->add('second_file', FileType::class, array('label' => 'Second file to clear', 'required' => false));

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => AmazonClearDocument::class
        ));
    }
}