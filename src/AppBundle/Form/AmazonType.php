<?php

namespace AppBundle\Form;

use AppBundle\Entity\AmazonDocument;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class AmazonType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('first_file', FileType::class, array('label' => 'Amazon file', 'required' => false))
            ->add('second_file', FileType::class, array('label' => 'Westfjord file', 'required' => false))
            ->add('third_file', FileType::class, array('label' => 'Trollkids file', 'required' => false))
            ->add('fourth_file', FileType::class, array('label' => 'Additional file', 'required' => false));

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => AmazonDocument::class
        ));
    }
}