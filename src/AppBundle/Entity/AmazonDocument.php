<?php

namespace AppBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Validator\Constraints as ExtensionAssert;

class AmazonDocument
{
    /**
     * @ExtensionAssert\AllowedExtensions(allowedExtensions={ "txt","csv" })
     */
    private $firstFile;

    /**
     * @ExtensionAssert\AllowedExtensions(allowedExtensions={ "txt","csv" })
     */
    private $secondFile;

    /**
     * @ExtensionAssert\AllowedExtensions(allowedExtensions={ "txt","csv" })
     */
    private $thirdFile;

    /**
     * @ExtensionAssert\AllowedExtensions(allowedExtensions={ "xls" })
     */
    private $fourthFile;

    public function getFirstFile()
    {
        return $this->firstFile;
    }

    public function setFirstFile($data)
    {
        $this->firstFile = $data;
    }

    public function getSecondFile()
    {
        return $this->secondFile;
    }

    public function setSecondFile($data)
    {
        $this->secondFile = $data;
    }

    public function getThirdFile()
    {
        return $this->thirdFile;
    }

    public function setThirdFile($data)
    {
        $this->thirdFile = $data;
    }

    public function getFourthFile()
    {
        return $this->fourthFile;
    }

    public function setFourthFile($data)
    {
        $this->fourthFile = $data;
    }
}