<?php

namespace AppBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Validator\Constraints as ExtensionAssert;

class AffiliDocument
{
    /**
     * @Assert\NotBlank(message="Please select file")
     * @ExtensionAssert\AllowedExtensions(allowedExtensions={ "xls","xlsx" })
     */
    private $firstFile;

    /**
     * @Assert\NotBlank(message="Please select file")
     * @ExtensionAssert\AllowedExtensions(allowedExtensions={ "xls","xlsx" })
     */
    private $secondFile;

    public function getFirstFile()
    {
        return $this->firstFile;
    }

    public function setFirstFile($data)
    {
        $this->firstFile = $data;
    }

    public function getSecondFile()
    {
        return $this->secondFile;
    }

    public function setSecondFile($data)
    {
        $this->secondFile = $data;
    }
}