<?php

namespace AppBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Validator\Constraints as ExtensionAssert;

class PdfDocument
{
    /**
     * @Assert\NotBlank(message="Please select PDF")
     * @ExtensionAssert\AllowedExtensions(allowedExtensions={ "pdf" })
     */
    private $pdfFile;

    public function getPdfFile()
    {
        return $this->pdfFile;
    }

    public function setPdfFile($data)
    {
        $this->pdfFile = $data;
    }
}