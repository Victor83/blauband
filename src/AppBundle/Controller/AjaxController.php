<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AjaxController extends Controller
{
    /**
     * @Route("/ajax", name="ajax_controller")
     */
    public function indexAction(Request $request)
    {

        if ($request->getMethod() == "GET"){
            $parameters = $request->query->all();
        } else {
            $parameters = $request->request->all();
        }

        if (!isset($parameters['action'])){

            $success = false;
            $message = "No action specified";

        } else {

            $action = $parameters['action'] . "Action";
            if (method_exists($this, $action)){
                $result = $this->$action($parameters);
            } else {
                $success = false;
                $message = "Internal system error";

                $result = [
                    'success' => $success,
                    'message' => $message,
                ];
            }

        }

        return new Response(json_encode($result));
    }

    public function sortAction($parameters)
    {
        $_SESSION['sort'][$parameters['sortType']] = "down";
        $_SESSION['sortType'] = $parameters['sortType'];

        if ($parameters['sort'] == "down"){
            $_SESSION['sort'][$parameters['sortType']] = "up";
        }

        $result = [
            'success' => true,
            'message' => "Set value " . $parameters['sortType'],
        ];

        return $result;
    }
}