<?php

namespace AppBundle\Controller;

use AppBundle\Entity\AffiliDocument;
use AppBundle\Entity\AmazonClearDocument;
use AppBundle\Entity\AmazonDocument;
use AppBundle\Form\AffiliType;
use AppBundle\Form\AmazonClearType;
use AppBundle\Form\AmazonType;
use AppBundle\Service\AmazonHelper;
use AppBundle\Service\FileHelper;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Service\FileHelper as AppFileHelper;
use AppBundle\Service\AffiliHelper as AffiliHelper;

class ToolsController extends Controller
{
    /**
     * @Route("/tools/{toolName}", name="tools")
     */
    public function indexAction(Request $request)
    {
        $toolName = $request->attributes->get('toolName');
        $actionFunction = 'process' . ucfirst($toolName) . 'Action';

        return $this->$actionFunction($request);
    }

	/**
	 * @Route("/tools/{toolName}/show/{toolAction}/{id}", name="tool_single_action")
	 */
	public function subSingleAction(Request $request) {

		$tool = $request->attributes->get('toolName');
		$action = $request->attributes->get('toolAction');

		$template = 'tools/' . $tool . '-' . $action . '-single.html.twig';

		return $this->renderTemplate($template);
	}

	/**
	 * @Route("/tools/{toolName}/show/{toolAction}", name="tool_action")
	 */
	public function subAction(Request $request) {

		$vars = [];

		$tool = $request->attributes->get('toolName');
		$action = $request->attributes->get('toolAction');

		$template = 'tools/' . $tool . '-' . $action . '.html.twig';

		$actionDir = $this->getParameter('temp_directory') . '/' . $tool;

		if (is_dir($actionDir)){

			$fileHelper = new FileHelper($this->container);

			// Set sorting order
            $sortName = $_SESSION['sort']['name'] = (isset($_SESSION['sort']['name'])) ? $_SESSION['sort']['name'] : "down";
            $sortCreate = $_SESSION['sort']['create'] = (isset($_SESSION['sort']['create'])) ? $_SESSION['sort']['create'] : "down";
            $_SESSION['sortType'] = (isset($_SESSION['sortType'])) ? $_SESSION['sortType'] : "name";

            // Get created files from directory
			$files = $fileHelper->getDirectoryFiles($tool, 20);

			if (empty($files)){
			    $files = null;
            }

			$paginator = $fileHelper->paginator;

            $vars       = [
				'files' => $files,
                'sort' => [
                    'name' => $sortName,
                    'create' => $sortCreate,
                ],
                'sortType' => [
                    $_SESSION['sortType'] => true
                ],
                'paginator' => $paginator,
			];
		}

		return $this->renderTemplate($template, $vars);
	}

    /**
     * @Route("/tools/amazon/clear", name="amazon_clear_action")
     */
    public function clearAction(Request $request)
    {
        $clearDocuments = new AmazonClearDocument();

        $form = $this->createForm(AmazonClearType::class, $clearDocuments);
        $form->handleRequest($request);

        $result = null;

        if ($form->isSubmitted() && $form->isValid()) {

            $firstFileFull  = null;
            $secondFileFull = null;

            /** @var \Symfony\Component\HttpFoundation\File\UploadedFile $firstFile */
            $firstFile = $clearDocuments->getFirstFile();
            /** @var \Symfony\Component\HttpFoundation\File\UploadedFile $secondFile */
            $secondFile = $clearDocuments->getSecondFile();

            if ($firstFile->getClientOriginalName() != $secondFile->getClientOriginalName()) {

                // Generate unique filenames
                if ($firstFile) {
                    $firstFileName = md5(uniqid()).'.'.AppFileHelper::getFileExtension($firstFile);
                }

                if ($secondFile) {
                    $secondFileName = md5(uniqid()).'.'.AppFileHelper::getFileExtension($secondFile);
                }

                // Move files to their directory
                $tempDirectory = $this->getParameter('temp_directory');
                if ($firstFile) {
                    $firstFile->move(
                        $tempDirectory,
                        $firstFileName
                    );
                }

                if ($secondFile) {
                    $secondFile->move(
                        $tempDirectory,
                        $secondFileName
                    );
                }

                $firstFileFull  = $tempDirectory.'/'.$firstFileName;
                $secondFileFull = $tempDirectory.'/'.$secondFileName;

                // Process necessary logic
                $params = [
                    'delimeters' => [
                        'first'  => "\t",
                        'second' => "\t",
                    ],
                    'charsets' => [
                        'first'  => 'Windows-1252',
                        'second' => 'Windows-1252',
                    ],
                ];

                $container   = $this->container;
                $amazonClear = new AmazonHelper($firstFileFull, $secondFileFull, null, null, $container, $params);
                $result      = $amazonClear->clearAmazonFiles();

                // Clean temporary directory
                if ($firstFile) {
                    unlink($firstFileFull);
                }

                if ($secondFile) {
                    unlink($secondFileFull);
                }

                $newExcel = new \PHPExcel();
                $newSheet = $newExcel->getActiveSheet();

                // Override data by sorted ones
                $newSheet->fromArray($result, null, 'A1', true);

                $fileName = 'amazon_'.date("Y_m_d_His").'.txt';

                header('Content-type: text/csv');
                header('Content-Disposition: attachment; filename="'.$fileName.'"');

                $objWriter = \PHPExcel_IOFactory::createWriter($newExcel, 'CSV');
                $objWriter->setDelimiter($amazonClear::AMAZON_SOURCE_DELIMETER);
                $objWriter->setEnclosure("");

                // Write file to the browser
                $objWriter->save('php://output');
                exit;
            } else {
                $result['error'] = 'You choose the same files';
            }
        }

        $template = 'tools/amazon-clear.html.twig';

        $vars = [
            'form' => $form->createView()
        ];

        $vars['errors'] = [];
        if (isset($result['error']) && !empty($result['error'])){
            $vars['errors'][] = $result['error'];
        }

        return $this->renderTemplate($template, $vars);
    }

    public function processAffiliAction(Request $request)
    {

        $affili = new AffiliDocument();

        $form = $this->createForm(AffiliType::class, $affili);
        $form->handleRequest($request);

        $action = $request->attributes->get('toolName');

        if ($form->isSubmitted() && $form->isValid()){

            /** @var \Symfony\Component\HttpFoundation\File\UploadedFile $firstFile */
            $firstFile = $affili->getFirstFile();
            /** @var \Symfony\Component\HttpFoundation\File\UploadedFile $secondFile */
            $secondFile = $affili->getSecondFile();

            // Generate unique filenames
            $firstFileName = md5(uniqid()) . '.' . AppFileHelper::getFileExtension($firstFile);
            $secondFileName = md5(uniqid()) . '.' . AppFileHelper::getFileExtension($secondFile);

            // Move files to their directory
            $moveDirectory = $this->getParameter('temp_directory');
            $firstFile->move(
                $moveDirectory,
                $firstFileName
            );
            $secondFile->move(
                $moveDirectory,
                $secondFileName
            );

            $firstFileFull = $moveDirectory . '/' . $firstFileName;
            $secondFileFull = $moveDirectory . '/' . $secondFileName;

            // Process necessary logic
            $affili = new AffiliHelper($firstFileFull, $secondFileFull);
            $affili->saveFile($moveDirectory);

            // Clean temporary directory
            unlink($firstFileFull);
            unlink($secondFileFull);

        }

        $template = 'tools/' . $action . '.html.twig';

	    $vars = [
		    'title' => "Affili.net helper",
		    'form'  => $form->createView(),
	    ];

	    return $this->renderTemplate($template, $vars);

    }

    public function processAmazonAction(Request $request)
    {
        $amazon = new AmazonDocument();

        $form = $this->createForm(AmazonType::class, $amazon);
        $form->handleRequest($request);

        $action = $request->attributes->get('toolName');

        if ($form->isSubmitted() && $form->isValid()){

	        $firstFileFull = null;
	        $secondFileFull = null;
	        $thirdFileFull = null;
	        $fourthFileFull = null;

            /** @var \Symfony\Component\HttpFoundation\File\UploadedFile $firstFile */
            $firstFile = $amazon->getFirstFile();
            /** @var \Symfony\Component\HttpFoundation\File\UploadedFile $secondFile */
            $secondFile = $amazon->getSecondFile();
            /** @var \Symfony\Component\HttpFoundation\File\UploadedFile $thirdFile */
            $thirdFile = $amazon->getThirdFile();
            /** @var \Symfony\Component\HttpFoundation\File\UploadedFile $fourthFile */
            $fourthFile = $amazon->getFourthFile();

            // Generate unique filenames

	        if ($firstFile) {
		        $firstFileName = md5( uniqid() ) . '.' . AppFileHelper::getFileExtension( $firstFile );
	        }

	        if ($secondFile) {
		        $secondFileName = md5( uniqid() ) . '.' . AppFileHelper::getFileExtension( $secondFile );
	        }

            if ($thirdFile){
                $thirdFileName = md5(uniqid()) . '.' . AppFileHelper::getFileExtension($thirdFile);
            }

	        if ($fourthFile) {
		        $fourthFileName = md5( uniqid() ) . '.' . AppFileHelper::getFileExtension( $fourthFile );
	        }

            // Move files to their directory
            $tempDirectory = $this->getParameter('temp_directory') . '/amazon/';
	        if ($firstFile) {
		        $firstFile->move(
			        $tempDirectory,
			        $firstFileName
		        );
	        }

	        if ($secondFile) {
		        $secondFile->move(
			        $tempDirectory,
			        $secondFileName
		        );
	        }

            if ($thirdFile){
                $thirdFile->move(
                    $tempDirectory,
                    $thirdFileName
                );
            }

	        if ($fourthFile) {
		        $fourthFile->move(
			        $tempDirectory,
			        $fourthFileName
		        );
	        }

	        if ($firstFile) {
		        $firstFileFull = $tempDirectory . $firstFileName;
	        }

	        if ($secondFile) {
		        $secondFileFull = $tempDirectory . $secondFileName;
	        }

            if ($thirdFile){
                $thirdFileFull = $tempDirectory . $thirdFileName;
            }

	        if ($fourthFile) {
		        $fourthFileFull = $tempDirectory . $fourthFileName;
	        }

            // Process necessary logic
	        $container = $this->container;
            $amazon = new AmazonHelper($firstFileFull, $secondFileFull, $fourthFileFull, $thirdFileFull, $container);
            $amazon->process();

            // Clean temporary directory
	        if ($firstFile) {
		        unlink( $firstFileFull );
	        }

	        if ($secondFile) {
		        unlink( $secondFileFull );
	        }

            if ($thirdFile){
                unlink($thirdFileFull);
            }

	        if ($fourthFile) {
		        unlink( $fourthFileFull );
	        }

        }

	    $template = 'tools/' . $action . '.html.twig';

	    $vars = [
		    'title' => "Amazonn helper",
		    'form' => $form->createView(),
	    ];

	    if (isset($amazon->temporaryData['files'])){
		    $vars['files'] = $amazon->temporaryData['files'];
	    }

	    return $this->renderTemplate($template, $vars);

    }

	public function renderTemplate($template, $vars = []) {

		try {

			return $this->render($template,$vars);

		} catch (\Exception $exception){

			return $this->render('default/error.html.twig',[
				'error' => "ERROR: " . $exception->getMessage()
			]);

		}

    }
}