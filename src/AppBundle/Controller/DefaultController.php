<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="welcome")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
            'user_auth' => false,
        ]);
    }

    public function headerAction(Request $request)
    {
        // action to generate header template
        $vars = [
            'logo_text' => 'Blauband Tools',
        ];
        return $this->render('default/header.html.twig', $vars);
    }

    public function footerAction(Request $request)
    {
        // action to generate footer template

        $vars = [
            'company_link' => 'https://blauband.com',
            'current_year' => date('Y'),
        ];
        return $this->render('default/footer.html.twig', $vars);
    }
}
