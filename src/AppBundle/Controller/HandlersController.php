<?php

namespace AppBundle\Controller;

use AppBundle\Service\ExcelHelper;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class HandlersController extends Controller
{

    private $writeDelimiter = ";";
    private $writeEnclosure = "\"";
    private $writerType = "Excel2007";
    private $temporaryDirectory = null;

	/**
	 * @Route("/download/{section}/{fileName}", name="download")
	 */
	public function downloadAction(Request $request) {

		$section = $request->attributes->get('section');

		$file = $request->attributes->get('fileName');
		$fullFileName = $this->getParameter('temp_directory') . '/' . $section . '/' . $request->attributes->get('fileName');

		header('Content-Type: application/octet-stream');
		header('Content-Transfer-Encoding: binary');
		header('Content-Disposition: attachment;filename="' . $file);
		header('Content-Length: '.filesize($fullFileName));
		header('Cache-Control: max-age=0');

		if (is_file($fullFileName)){
			echo file_get_contents($fullFileName);
			exit;
		}


	}

    /**
     * @Route("/edit/{action}/{fileName}", name="edit_update_file")
     * @Method("GET")
     */
    public function editAction(Request $request) {

        $action = $request->attributes->get('action');

        $file = $request->attributes->get('fileName');

        $fullFileName = $this->getParameter('temp_directory') . '/' . $action . '/' . $file;

        if (strpos($file, 'versendet') === 0){
            $fileGroup = 'versendet';
        } else {
            $fileGroup = substr($file, 0, strpos($file, '_'));
        }
        $fileType  = $this->getParameter($fileGroup . "_type");

        $container = $this->container;
        $temporaryDirectory = $container->getParameter('temp_directory') . '/' . $action;
        $this->setTemporaryDirectory($temporaryDirectory);
        $helper = new ExcelHelper($container, $temporaryDirectory);

        if ($fileType == 'Excel2007'){
            $fileData = $helper->prepareFile($fullFileName, 'xls');
        } else {
            $fileDelimiter = $this->getParameter($fileGroup . "_delimiter");
            $fileEnclosure = $this->getParameter($fileGroup . "_enclosure");
            $fileData = $helper->prepareFile($fullFileName, $fileType, $fileDelimiter);
        }

        $template = 'tools/' . $action . '-edit-single.html.twig';

        $table = $helper->createTableFromData($fileData);

        if (empty($table)){
            $table = null;
        }

        if (!empty($fileData)){
            $vars = [
                'file' => $file,
                'table' => $table,
            ];
        } else {
            $vars = [

            ];
        }

        return $this->renderTemplate($template, $vars);
    }

    /**
     * @Route("/edit/{section}/{fileName}", name="edit_file")
     * @Method("POST")
     */
    public function editAmazonAction(Request $request) {

        $output = [];

        $output['output'] = "sweetalert";
        $output['title'] = "System information";
        $success = false;

        if ($request->getMethod() == "POST"){

            $section = $request->attributes->get('section');

            $file = $request->attributes->get('fileName');

            $fullFileName = $this->getParameter('temp_directory') . '/' . $section . '/' . $file;

            if (strpos($file, 'versendet') == 0){
                $fileGroup = 'versendet';
            } else {
                $fileGroup = substr($file, 0, strpos($file, '.'));
            }
            $fileType  = $this->getParameter($fileGroup . "_type");

            $container = $this->container;
            $temporaryDirectory = $container->getParameter('temp_directory') . $section;
            $this->setTemporaryDirectory($temporaryDirectory);

            $helper = new ExcelHelper($container, $temporaryDirectory);

            if ($fileType == 'Excel2007'){
                $fileData = $helper->prepareFile($fullFileName, 'xls');
            } elseif ($fileType == 'CSV') {
                $fileDelimiter = $this->getParameter($fileGroup . "_delimiter");
                $this->setWriteDelimiter($fileDelimiter);
                $fileEnclosure = $this->getParameter($fileGroup . "_enclosure");
                $this->setWriteEnclosure($fileEnclosure);
                $fileData = $helper->prepareFile($fullFileName, $fileType, $this->writeDelimiter);
            }

            $parameters = $request->request->all();
            $fullFileName = $this->getParameter('temp_directory') . '/' . $section . '/' . $file;

            if (isset($parameters['row']) && isset($parameters['col']) && isset($parameters['value'])) {

                $cell = \PHPExcel_Cell::stringFromColumnIndex($parameters['col']) . $parameters['row'];
                $activeSheet = $fileData->getActiveSheet();
                if ($activeSheet->setCellValue($cell, $parameters['value'])){
                    if ($this->saveUpdatedFile($fileData, $fullFileName, $fileType)){
                        $output['success'] = true;
                        $output['text'] = "Value updated successfully";
                        $output['type'] = "success";
                    } else {
                        $output['success'] = false;
                        $output['text'] = "An error was occured while saving file";
                        $output['type'] = "error";
                    }
                } else {
                    $output['success'] = false;
                    $output['text'] = "An error was occured while updating value";
                    $output['type'] = "error";
                }
            } else {
                $output['success'] = false;
                $output['text'] = "An error was occured while updating value";
                $output['type'] = "error";
            }
        } else {
            $type = "error";
            $message = "Wrong request method";
        }

        $result = [
            'success' => $success,
            'output' => $output,
            'returnUrl' => $request->server->get("HTTP_REFERER"),
        ];

        return new Response(json_encode($result));
        exit;
    }

    /**
     * @Route("/delete/{section}/{fileName}", name="delete_file")
     */
    public function deleteAction(Request $request) {

        $section = $request->attributes->get('section');

        $file = $request->attributes->get('fileName');

        $success = false;
        $output = [];

        $output['output'] = "sweetalert";
        $output['title'] = "System information";
        $output['text'] = '';
        $output['type'] = 'error';
        $output['showConfirmButton'] = false;
        $output['windowReload'] = true;

        if ($request->getMethod() == "GET"){
            $fullFileName = $this->getParameter('temp_directory') . '/' . $section . '/' . $file;
            if (is_file($fullFileName)){
                if (unlink($fullFileName)) {
                    $success = true;
                    $output['text'] = "File \"" . $file . "\" deleted successfully";
                    $output['type'] = "success";
                } else {
                    $output['text'] = "An error was occured whle deleting file";
                }
            } else {
                $output['text'] = "File doesn't exists";
            }
        } else {
            $output['text'] = "Wrong request method";
        }

        $result = [
            'success' => $success,
            'output' => $output,
            'returnUrl' => $request->server->get("HTTP_REFERER"),
        ];

        return new Response(json_encode($result));
        exit;
    }

    public function renderTemplate($template, $vars = []) {

        try {

            return $this->render($template,$vars);

        } catch (\Exception $exception){

            return $this->render('default/error.html.twig',[
                'error' => "ERROR: " . $exception->getMessage()
            ]);

        }

    }

    public function saveUpdatedFile($object, $fileName, $fileType)
    {
        if (!in_array($fileType, ['CSV', 'Excel2007'])){
            return false;
        }

        $success = true;

        $this->setWriterType($fileType);
        $objWriter = \PHPExcel_IOFactory::createWriter($object, $fileType);

        try {
            $this->saveResultFile($objWriter, $fileName);
        } catch (Exception $exception) {
            $success = false;
        }

        return $success;
    }

    public function setWriteDelimiter($delimiter)
    {
        $this->writeDelimiter = $delimiter;
    }

    public function setWriteEnclosure($enclosure)
    {
        $this->writeEnclosure = $enclosure;
    }

    public function setWriterType($type)
    {
        $this->writerType = $type;
    }

    public function setTemporaryDirectory($directory)
    {
        $this->temporaryDirectory = $directory;
    }

    public function saveResultFile($writer, $fileName) {

        if ($this->writerType == 'CSV'){
            $writer->setDelimiter($this->writeDelimiter);
            $writer->setEnclosure($this->writeEnclosure);
            $writer->save($fileName);
        } elseif ($this->writerType == 'Excel2007') {
            $writer->save($fileName);
        }
    }
}