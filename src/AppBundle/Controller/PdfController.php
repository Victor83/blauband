<?php

namespace AppBundle\Controller;

use AppBundle\Entity\PdfDocument;
use AppBundle\Form\PdfType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Service\FileHelper as AppFileHelper;

class PdfController extends Controller
{
    /**
     * @Route("/pdf", name="pdf_reader")
     */
    public function indexAction(Request $request)
    {
        $actionDir = $this->getParameter('temp_directory') . '/pdf/';

        return $this->render('pdf/pdf.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
            'user_auth' => false,
        ]);
    }

    /**
     * @Route("/pdf/read/", name="read_pdf")
     */
    public function readAction(Request $request)
    {
        $pdf = new PdfDocument();

        $form = $this->createForm(PdfType::class, $pdf);
        $form->handleRequest($request);

        $pdfPagesData = [];
        if ($form->isSubmitted() && $form->isValid()) {

            /** @var \Symfony\Component\HttpFoundation\File\UploadedFile $pdfFile */
            $pdfFile = $pdf->getPdfFile();

            if ($pdfFile) {
                $pdfFileName = md5( uniqid() ) . '.' . AppFileHelper::getFileExtension( $pdfFile );
            }

            // Move files to their directory
            $tempDirectory = $this->getParameter('temp_directory') . '/pdf/';
            if ($pdfFile) {
                $pdfFile->move(
                    $tempDirectory,
                    $pdfFileName
                );
            }

            if ($pdfFile) {
                $pdfFileFull = $tempDirectory . $pdfFileName;
            }

            $parser = new \Smalot\PdfParser\Parser();

            $pdfParsed = $parser->parseFile($pdfFileFull);

            $pdfPages = $pdfParsed->getObjects();

            foreach ($pdfPages as $pdfPage) {
                $pdfPagesData[] = $pdfPage->getText();
            }
        }
        #$file = $request->attributes->get('file');

        $template = 'pdf/read.html.twig';

        $vars = [
            'title' => "Affili.net helper",
            'form'  => $form->createView(),
            'pages'  => $pdfPagesData,
        ];

        return $this->render($template,$vars);
    }
}
