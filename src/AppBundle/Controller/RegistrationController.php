<?php
namespace AppBundle\Controller;

use AppBundle\Entity\User,
    AppBundle\Form\UserType,
    Sensio\Bundle\FrameworkExtraBundle\Configuration\Route,
    Symfony\Bundle\FrameworkBundle\Controller\Controller,
    Symfony\Component\HttpFoundation\Request;

class RegistrationController extends Controller {

    /**
     * @Route("/register", name="register")
     */
    function registerAction(Request $request){

        // Create new blank user and process form
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            // Encode new user password
            $encoder = $this->get('security.password_encoder');
            $password = $encoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);

            // Set user role
            $user->setRole('ROLE_USER');

            //Save
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('login');

        }

        return $this->render('auth/register.html.twig',[
            'form' => $form->createView()
        ]);

    }
}
